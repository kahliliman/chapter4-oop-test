class Game {
    constructor(team1, team2) {
        this.team1 = team1;
        this.team2 = team2;
    }
}

class Team {
    constructor(name, lineup) {
        this.name = name;
        this.lineup = lineup;
        this.score = 0;
    }

    scoreGoal(player) {
        for (let i = 0; i < this.lineup.length; i++) {
            if (this.lineup[i].getName() == player.getName()) {
                this.lineup[i].setGoalScored();
            }
        }
        this.score += 1;
    }

    getName() {
        return this.name;
    }
}

class Player {
    constructor(name, number) {
        this.name = name;
        this.number = number;
        this.goalScored = 0;
    }

    setGoalScored() {
        this.goalScored += 1;
    }

    setName(name) {
        this.name = name;
    }

    setNumber(number) {
        this.number = number;
    }

    getName() {
        return this.name;
    }

}

class GoalKeeper extends Player {
    constructor(name, number) {
        super(name, number)
    }

    saveBall() { //with hands
        console.log(`${this.name} saved the ball`);
    }
}

// Initiate Player, Team, and Game 

const player1 = new Player("iman", 27);
const player2 = new Player("rizki", 10);
const player3 = new Player("alfian", 11);
const player4 = new Player("nicko", 9);
const player5 = new GoalKeeper("akmal", 1);

const player6 = new Player("Firman", 26);
const player7 = new Player("Dito", 14);
const player8 = new Player("Rodger", 13);
const player9 = new Player("Jarwo", 8);
const player10 = new GoalKeeper("Ferdi", 2);

const team1 = new Team('Pekayon Squad', [player1, player2, player3, player4, player5]);
const team2 = new Team('Maung Bandung', [player6, player7, player8, player9, player10]);

const game = new Game(team1, team2);

team1.scoreGoal(player3);
team1.scoreGoal(player4);
team2.scoreGoal(player8);
team2.scoreGoal(player8);
team2.scoreGoal(player8);


