# BANK TELLER

Scenario: Adi, Budi, Charli, Didi datang secara berurutan sebagai nasabah masing-masing memiliki saldo 100.000, 50.000, 80.000, 300.000. Alamat mereka di Jaksel, Jaktim, Bandung, Malang. Charli akan transfer 50.000 ke Adi. Budi akan menarik saldo 50.000, Adi akan cek saldo, Didi akan menabung 700.000.
Security bernama John akan mengurus nomor antrian. Teller bernama Maria akan melayani nasabah.

Intention payload:
let intent = { "action" : "withdraw", "amount" : 10000, "recipient" : member }

intention list:
action > withdraw, deposit, transfer, checkBalance
amount > apabila actionnya adalah salah satu dari withdraw, deposit, atau transfer maka harus lebih dari 0. Sedangkan untuk checkBalance boleh 0
recipient > kalau actionnya transfer maka recipient tidak boleh null, sisanya boleh null

