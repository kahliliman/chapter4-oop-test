class Vehicle {
    constructor(licensePlateNum, brand, vehicleCategory) {
        this.licensePlateNum = licensePlateNum;
        this.brand = brand;
        this.vehicleCategory = vehicleCategory;
        this.ticketNum;
        this.parkingStatus = 0;
    }

    setParkingStatus(val) {
        this.parkingStatus = val;
    }

    setTicketNum(licensePlateNum, time) {
        this.ticketNum = licensePlateNum + time;
    }

    getTicketNum() {
        return this.ticketNum;
    }

    getParkingStatus() {
        return this.parkingStatus;
    }

    getVehicleCategory() {
        return this.vehicleCategory;
    }

    getLicensePlateNum() {
        return this.licensePlateNum;
    }
}

class ParkingArea {
    constructor() {
        this.MAX_CAR_CAPACITY = 15;
        this.MAX_MOTORCYCLE_CAPACITY = 30;
        this.vehicleTicketList = []
        this.parkedCarCount = 0;
        this.parkedMotorcycleCount = 0;
    }

    serveTicket(vehicle, time) {
        vehicle.setParkingStatus(1);
        this.parkedCarCount += 1;
        vehicle.setTicketNum(vehicle.getLicensePlateNum(), time);
        this.vehicleTicketList.push({ "license_plate": vehicle.getLicensePlateNum(), "ticket_num": vehicle.getTicketNum() });
    }

    vehicleIn(vehicle, time) {
        if (vehicle.getVehicleCategory() == 'car') {
            if (this.parkedCarCount <= this.MAX_CAR_CAPACITY) {
                this.serveTicket(vehicle, time);
            } else {
                console.log('Parking is full');
            }
        } else {
            if (this.parkedMotorcycleCount <= this.MAX_MOTORCYCLE_CAPACITY) {
                this.serveTicket(vehicle, time);
            } else {
                console.log('Parking is full');
            }
        }

    }

    vehicleOut(vehicle) {
        // check ticket di vehicle dengan ticket di ticket list
        let ticket = vehicle.getTicketNum();
        let ticketFromList;
        for (let i = 0; i < this.vehicleTicketList.length; i++) {
            if (this.vehicleTicketList[i]["license_plate"] == vehicle.getLicensePlateNum()) {
                ticketFromList = this.vehicleTicketList[i]["ticket_num"]
            }
        }
        if (ticket == ticketFromList) {
            if (vehicle.vehicleCategory == 'car') {
                this.parkedCarCount -= 1;
                vehicle.setParkingStatus(0);
            } else {
                this.parkedMotorcycleCount -= 1;
                vehicle.setParkingStatus(0);
            }

        } else {
            console.log('Ticket doesn\'t match');
        }
    }

}

const odang = new ParkingArea();

const mobil1 = new Vehicle('B1234', 'Toyota', 'car');
const mobil2 = new Vehicle('B5678', 'Honda', 'car');
const motor1 = new Vehicle('B4321', 'Suzuki', 'motorcycle');
const motor2 = new Vehicle('B8765', 'Honda', 'motorcycle');

console.log(odang.parkedCarCount);
console.log(odang.vehicleTicketList);
console.log(mobil1.ticketNum);
console.log(mobil1.parkingStatus);

odang.vehicleIn(mobil1, '20201011');

console.log(odang.parkedCarCount);
console.log(odang.vehicleTicketList);
console.log(mobil1.ticketNum);
console.log(mobil1.parkingStatus);

odang.vehicleOut(mobil1);

console.log(odang.parkedCarCount);
console.log(odang.vehicleTicketList);
console.log(mobil1.ticketNum);
console.log(mobil1.parkingStatus);