
class Person {
    constructor(name, address) {
        this.name = name;
        this.address = address;
    }

    introduction() {
        console.log(`hai, nama saya ${this.name}, alamat ${this.address}`);
    }

    getName() {
        return this.name;
    }

    getAddress() {
        return this.address;
    }
}


class Customer extends Person {
    constructor(name, address, intent, accountBalance) {
        super(name, address)
        this.accountBalance = accountBalance;
        this.intent = intent;
        this.queueNumber;
    }

    getQueueNumber() {
        return this.queueNumber;
    }

    setQueueNumber(num) {
        this.queueNumber = num;
    }

    getAccountBalance() {
        return this.accountBalance;
    }

    setAccountBalance(num) {
        this.accountBalance = num;
    }

    getIntention(key) {
        return this.intent[key];
    }

}


class Teller extends Person {
    constructor(name, address) {
        super(name, address)
    }

    serve(customer, queueMachine) {
        if (customer.getQueueNumber() != queueMachine.getCurrentQueue()) {
            console.log(`Maaf ${customer.name}, ini bukan saatnya saya melayani anda`);
        } else {
            let amount = customer.getIntention('amount');
            let balance = customer.getAccountBalance();

            if (customer.getIntention('action') == 'withdraw') {
                if (balance >= amount) {
                    customer.setAccountBalance(balance - amount);
                    console.log(`Anda berhasil menarik uang sejumlah ${amount}. Saldo anda sekarang adalah ${balance - amount}`);
                } else {
                    console.log('Saldo anda tidak cukup');
                }
            } else if (customer.getIntention('action') == 'deposit') {
                customer.setAccountBalance(balance + amount);
                console.log(`Anda berhasil mendeposit uang sejumlah ${amount}. Saldo anda sekarang adalah ${balance + amount}`);
            } else if (customer.getIntention('action') == 'transfer') {
                if (balance >= amount) {
                    if (customer.getIntention('recipient')) {
                        let recipient = customer.getIntention('recipient');
                        let recipientBalance = recipient.getAccountBalance();
                        recipient.setAccountBalance(recipientBalance + amount);
                        customer.setAccountBalance(balance - amount);
                        console.log(`Anda berhasil transfer uang sejumlah ${amount} kepada ${recipient.getName()}. Saldo anda sekarang adalah ${balance - amount}`);
                    } else {
                        console.log('Anda tidak menginput nama penerima transfer');
                    }
                } else {
                    console.log('Saldo anda tidak cukup');
                }
            } else {
                console.log(`Saldo anda ${amount}`);
            }

            queueMachine.shiftCurrentQueue();
        }
    }
}


class Security extends Person {
    constructor(name, address) {
        super(name, address)
    }

    register(customer, queueMachine) {
        if (!customer.getQueueNumber()) {
            console.log(`silakan ambil nomor antrian ${customer.getName()}`);
            let num = queueMachine.getQueueNumber();
            queueMachine.currentQueue.push(num);
            queueMachine.incrementQueueNumber();
            customer.setQueueNumber(num);
        } else {
            console.log(`silakan mengantri`);
        }
    }
}



class QueueMachine {
    constructor() {
        this.queueNumber = 1;
        this.currentQueue = [];
    }

    incrementQueueNumber() {
        this.queueNumber += 1;
    }

    setQueueNumber(num) {
        this.queueNumber = num;
    }

    setCurrentQueue(num) {
        this.currentQueue.push(num);
    }

    shiftCurrentQueue() {
        this.currentQueue.shift();
    }

    getQueueNumber() {
        return this.queueNumber;
    }

    getCurrentQueue() {
        return this.currentQueue[0];
    }

}

const customer1 = new Customer('Adi', 'Jaksel', { 'action': 'checkBalance', 'amount': 0, 'recipient': null }, 100000)
const customer2 = new Customer('Budi', 'Jaktim', { 'action': 'withdraw', 'amount': 50000, 'recipient': null }, 50000)
const customer3 = new Customer('Charli', 'Bandung', { 'action': 'transfer', 'amount': 50000, 'recipient': customer1 }, 80000)
const customer4 = new Customer('Didi', 'Malang', { 'action': 'deposit', 'amount': 700000, 'recipient': null }, 300000)

const security = new Security('John', 'Jogja');
const teller = new Teller('Maria', 'Padang');

const qMachine = new QueueMachine();

security.register(customer1, qMachine);
security.register(customer2, qMachine);
security.register(customer3, qMachine);
security.register(customer4, qMachine);

teller.serve(customer1, qMachine);
teller.serve(customer2, qMachine);
teller.serve(customer3, qMachine);
teller.serve(customer4, qMachine);



