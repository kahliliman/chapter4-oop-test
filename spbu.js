class Station {
    constructor(number, gasolineAmount) {
        this.number = number;
        this.gasolineAmount = gasolineAmount;
    }

    setNumber(number) {
        this.number = number;
    }

    getNumber() {
        return this.number;
    }

    setGasolineAmount(gasolineAmount) {
        this.gasolineAmount = gasolineAmount;
    }

    getGasolineAmount() {
        return this.gasolineAmount;
    }
}

class Person {
    constructor(name) {
        this.name = name;
    }

    greeting() {
        console.log(`Halo nama saya ${this.name}`);
    }

    setName(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

class Employee extends Person {
    constructor(name) {
        super(name);
        this.isRegistered;
    }

    setIsRegistered(bool) {
        this.isRegistered = bool;
    }

    getIsRegistered() {
        return this.isRegistered;
    }

    refillGasoline(customer, requestedGasolineAmount, station) {
        if (this.isRegistered) {
            this.greeting();
            if (requestedGasolineAmount <= station.getGasolineAmount()) {
                let stationFinalAmount = station.getGasolineAmount() - requestedGasolineAmount;
                let customerFinalAmount = customer.getGasolineAmount() + requestedGasolineAmount;

                station.setGasolineAmount(stationFinalAmount);
                customer.setGasolineAmount(customerFinalAmount);
                customer.incrementTransactionCount();

                if (customer.getTransactionCount >= 4) {
                    customer.setIsMember(true);
                    console.log(`Selamat, anda mendapat benefit member yaitu discount 10% dengan minimum transaksi 100rb dan 2.5% dengan minimum 20rb`);
                }

                console.log(`${customer.getName()} telah berhasil mengisi bensin sejumlah ${requestedGasolineAmount}. Jumlah bensin sekarang adalah ${customer.getGasolineAmount()}`);


            } else {
                console.log(`Mohon maaf ${customer.getName()}.Pom nomor ${station.getNumber()} tidak cukup bensin. Silakan ke pom selanjutnya.`)
            }

        } else {
            console.log(`${this.name} tidak terdaftar sebagai karyawan sehingga tidak dapat melakukan transaksi`);
        }

    }


}

class Customer extends Person {
    constructor(name, gasolineAmount, distancePerFuel) {
        super(name);
        this.gasolineAmount = gasolineAmount;
        this.transactionCount = 0;
        this.isMember = false;
        this.distancePerFuel = distancePerFuel;
    }

    ride(distance) {
        let eligibleDistance = this.distancePerFuel * this.gasolineAmount;

        this.gasolineAmount = (distance > eligibleDistance) ? 0 : (this.gasolineAmount - (distance / this.distancePerFuel));

        let coveredDistance = distance > eligibleDistance ? eligibleDistance : distance;

        console.log(`${this.name} berkendara sejauh ${coveredDistance} km. Sisa bensin ${this.gasolineAmount}`);
    }

    incrementTransactionCount() {
        this.transactionCount += 1;
    }

    getTransactionCount() {
        return this.transactionCount;
    }

    setIsMember(bool) {
        this.isMember = bool;
    }


    setGasolineAmount(gasolineAmount) {
        this.gasolineAmount = gasolineAmount;
    }

    getGasolineAmount() {
        return this.gasolineAmount;
    }
}

class Owner extends Person {
    constructor(name) {
        super(name);
        this.employeeList = [];
    }

    addEmployee(employee) {
        employee.setIsRegistered(true);
        this.employeeList.push(employee.getName());
    }

    getEmployeeList() {
        return this.employeeList;
    }
}

const stationOne = new Station(1, 25);
const stationTwo = new Station(2, 100);

const akmal = new Customer('Akmal', 20, 5);
const Fauzan = new Customer('Fauzan', 10, 10);

const taslina = new Owner('Taslina');

const john = new Employee('John');

taslina.addEmployee(john);

john.refillGasoline(akmal, 30, stationOne); // Refill tapi kapasitas tidak cukup
john.refillGasoline(akmal, 30, stationTwo); // Refill tapi kapasitas cukup

akmal.ride(100);

taslina.getEmployeeList();